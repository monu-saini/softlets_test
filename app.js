import express from 'express';
import next from 'next';
import morgan from 'morgan';
import compression from 'compression';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import setupApi from './api';
import Config from './configrations';
import { ApolloServer, AuthenticationError, ApolloError } from 'apollo-server-express';
import { typeDefs, resolvers} from './api/graphql';
import Models from './api/models';
import { authMiddleware } from './api/authentication';
import { universalFunctions, responseMessages } from './api/utils'
import setUpGridFs from './api/gridFs';

const dev = process.env.NODE_ENV !== 'production';
const port = Config.appConfig.get('/port/PORT', {
    env: process.env.NODE_ENV
});

const app = next({ dev, quiet: false });


const apploServer = new ApolloServer({ 
    typeDefs, 
    resolvers, 
    context: async ({ req }) => {
        try {
            let token = req.headers.token || req.query.token || req.cookies.token;
            if(token) {
                let userInfo = await authMiddleware(token);
                if(userInfo) {
                    userInfo = JSON.parse(JSON.stringify(userInfo));
                    return {
                        me: userInfo,
                        models: Models
                    }
                } else {
                    throw new AuthenticationError(responseMessages.UNAUTHORIZED);
                }
            } else {
                throw new AuthenticationError(responseMessages.UNAUTHORIZED);
            }
        } catch(error) {
            console.log('error', error)
           throw new ApolloError(responseMessages.FAILED, 500);
        }
    }
});

app.prepare().then(() => {
    const server = express();

    if (!dev) {
        server.use(compression());
    }

    server.use('/static', express.static(__dirname + '/static'));
    server.use(cookieParser());
    server.use(morgan('dev'));
    server.use(bodyParser.json());
    server.use(
        bodyParser.urlencoded({
            extended: false,
        })
    );

    setupApi(server);

    setUpGridFs(server)

    apploServer.applyMiddleware({ app: server, path: '/api/graphql' });

    server.get('*', async (req, res) => {
        try {
            let token = req.headers.token || req.query.token || req.cookies.token;
            if(token) {
                let userModel = await authMiddleware(token);
                if(!userModel) {
                    return app.render(req, res, '/login');
                } else {
                    return app.render(req, res, req.url);
                }
            } else {
                return app.render(req, res, '/login');
            }

        } catch(error) {
            return universalFunctions.sendError(error, res)
        }
    });

    

    server.listen(port, err => {
        if (err) {
            throw err;
        }
        console.log(`Running on localhost:${port}`);
        console.log(`GraphQL Server is Running on localhost:${port}${apploServer.graphqlPath}`);
    });
});
