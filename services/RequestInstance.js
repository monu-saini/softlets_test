import http from 'superagent';

const fire = (method, url) => {
    // Here We can write the code to set headers for authentication in case of JWT token authtication
    return http[method](url);
}

let Agent = {
    fire
}
export default Agent;
