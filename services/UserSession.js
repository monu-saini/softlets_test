import Agent from './RequestInstance';

module.exports = {
    login(payload) {
        return Agent
          .fire('post', '/api/login')
          .send(payload)
    }
}