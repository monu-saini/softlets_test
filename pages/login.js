import React, { Component } from 'react';
import UserSession from '../services/UserSession';
import Cookies from 'universal-cookie';
const cookie = new Cookies();
import './styles/loginPage.less';


export default class AuthLoginForm extends Component {
  constructor(props) {
    super(props);
    this.state =  {
      email: '',
      password: '',
      errorMessage: ''
    }
  }

  _handleChange = e => {
    this.setState({ 
      [e.target.name]: e.target.value 
    });
  };

  _handleSubmit = async e => {
    e.preventDefault();
    try {
      this.setState({
        errorMessage: ''
      })
      let { email, password } = this.state;
      let resposne = await UserSession.login({email, password });
      resposne = resposne.body;
      if(resposne.statusCode !== 200) {
        this.setState({
          errorMessage: resposne.message
        })
      } else {
        cookie.set('token', resposne.data.token, { path: '/' });
        this.props.url.push('/')

      }
    } catch (error) {
      console.log('[Error]', error)
    }
  }

  render() {
    let { errorMessage } = this.state;
    return (
        <div className="log-form">
            <h2>Login to your account</h2>
            {
              errorMessage ? <p className="error">{errorMessage}</p> : null
            }
            <form onSubmit={this._handleSubmit}> 
              <input 
                type="text" 
                name="email"
                placeholder="Email" 
                value={this.state.email}
                onChange={this._handleChange}
              />
              <input 
                type="password" 
                name="password" 
                placeholder="password" 
                value={this.state.password}
                onChange={this._handleChange}
              />
              <button 
                type="submit" 
                className="btn"
                // disabled={!this.isDirty() || !this.isValid()}
              >Login</button>
            </form>
      </div>
    );
  }
}

