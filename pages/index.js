import ApolloClient from 'apollo-client';
import { gql } from "apollo-boost";
import { ApolloProvider } from '@apollo/react-hooks';
import React, { Component } from 'react';
import { useQuery } from '@apollo/react-hooks';
import fetch from 'node-fetch'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
const API_URL = 'http://localhost:8080/api/graphql'

const client = new ApolloClient({
    link: createHttpLink({
        uri: API_URL,
        fetch: fetch,
    }),
    cache: new InMemoryCache(),
})

export default class TodoList extends Component {
    constructor() {
        super();
        this.state = {
            list: [],
            isLoading: true,
            error: false,
            title: ""
        }
    }

    componentDidMount() {
        this.shoHideLoader(true);
        client
            .query({
                query: gql`
            {
                todos {
                    title
                    id
                    isCompleted
                }
            }
            `
            })
            .then(result => {
                console.log('result', result);
                this.shoHideLoader(false);
                this.setState({ list: result.data.todos })
            })
            .catch((error) => {
                console.log('error', error);
                this.shoHideLoader(false);
                this.setState({ error: true })
            });
    }

    shoHideLoader(value) {
        this.setState({
            isLoading: value
        })
    }

    _handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    _handleAddTodo = e => {
        e.preventDefault();
        let { title } = this.state;
        title = title.toString();
        const ADD_TODO = gql`
                mutation {
                    createTodo(title:"${title}") {
                        id
                        title,
                        isCompleted
                    }
                }
            `;

            
            client
            .mutate({
                mutation: ADD_TODO
            })
            .then(result => {
                console.log('result', result);
                let { list } = this.state
                list.push(result.data.createTodo)
                this.setState({ list , title: ""})
            })
            .catch((error) => {
                this.shoHideLoader(false);
            });    
    }

    _handleTaskComplated(index, e) {
        let value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        let { list } = this.state
        let obj = list[index];
        obj.isCompleted = value;
        const updateTodo = gql`
            mutation {
                updateTodo (id:${obj.id}, title: "${obj.title}", isCompleted: ${obj.isCompleted}) {
                    id
                    title
                    isCompleted
                }
            }
        `;

        client
            .mutate({
                mutation: updateTodo
            })
            .then(result => {
                let { list } = this.state
                list[index] = result.data.updateTodo
                this.setState({ list , title: ""})
            })
            .catch((error) => {
                console.log('error', error);
                this.shoHideLoader(false);
            }); 
    }

    _handleDeleteTodo(index, e) {
        let { list } = this.state
        let obj = list[index];
        const deleteTodo = gql`
            mutation {
                deleteTodo (id:${obj.id}) {
                    id
                    title
                    isCompleted
                }
            }
        `;

        client
            .mutate({
                mutation: deleteTodo
            })
            .then(result => {
                let { list } = this.state
                // list[index] = result.data.updateTodo
                list.splice(index, 1)
                this.setState({ list , title: ""})
            })
            .catch((error) => {
                console.log('error', error);
                this.shoHideLoader(false);
            }); 
    }

    render() {
        let { list, isLoading, error } = this.state;
        if (error) return <p>Error :(</p>;
        if (isLoading) return <p>Loading...</p>;


        return (
            <div>
                <h2>Todo List</h2>
                <input
                    type="text"
                    name="title"
                    value={this.state.title}
                    onChange={this._handleChange}
                />
                <button
                    disabled={!this.state.title ? "disabled" : ""}
                    onClick={this._handleAddTodo}
                >Add</button>
                {list && list.length > 0 ? list.map(({ id, title, isCompleted }, index) => {
                    return <div key={id}>
                        <p>
                            {title}
                            <input
                                type="checkbox"
                                checked={isCompleted ? "checked" : ""}
                                onChange={this._handleTaskComplated.bind(this, index)}
                            />
                            <p 
                                onClick={this._handleDeleteTodo.bind(this, index)}
                                style={{
                                    display: "inline-block",
                                    marginLeft: "11px",
                                    cursor: "pointer"
                                }}
                            >X</p>
                        </p>
                    </div>
                }) : <p>Emplty List</p>}
            </div>

        );
    }
}

// export default class TodoListing extends Component {



//     render() {

//         return (
//             <ApolloProvider client={client}>
                // <div>
                //     <h2>Todo List</h2>
                //     <TodoList />
                // </div>
//             </ApolloProvider>
//         );
//     }
// }