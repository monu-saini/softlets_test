module.exports = {
    SUCCESS:'Success',
    DB_ERROR: 'Something went wrong.',
    DUPLICATE: 'Duplicate entry.',
    EMAIL_ALREADY_EXISTS:'This user is already registered with us',
    APP_ERROR: 'Application error occurred.',
    INVALID_OBJECT_ID: 'Invalid ID provided.',
    DEFAULT: 'Something went wrong.',
    INVALID_CREDENTIALS: "Incorrect credentials.",
    FAILED: "Failed",
    USER_NOT_FOUND: 'User does not exist',
    LOGIN_SUCCESSFULLY: "You are successfully login.",
    LOGOUT_SUCCESS: "You are successfully logged out",
    UNAUTHORIZED: 'not authenticated',
    NOT_FOUND: "Not Found.",
    FILE_UPLOADED_SUCCESSFULLY: "File Uploaded Successfully.",
    FILE_FETCHED_SUCCESSFULLY: "Files fetched Successfully",
    FILE_NOT_FOUND: "File not found."
}