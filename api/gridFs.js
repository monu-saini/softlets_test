import mongoose from 'mongoose';
import multer from 'multer';
import GridFsStorage from 'multer-gridfs-storage';
import Grid from 'gridfs-stream';
import Config from '../configrations';
import util from 'util';
import { universalFunctions, responseMessages } from './utils';

const connectingString = Config.appConfig.get('/mongo/CONNECTING_STRING', {
    env: process.env.NODE_ENV
});

mongoose.connect(connectingString);

const conn = mongoose.connection;
Grid.mongo = mongoose.mongo;
const gfs = Grid(conn.db);

const storage = GridFsStorage({
    gfs: gfs,
    filename(req, file, cb) {
        const datetimestamp = Date.now();
        cb(
            null,
            `${file.originalname.split('.').length - 1}${file.fieldname}-${datetimestamp}.${file.originalname.split('.')[file.originalname.split('.').length - 1]}`
        );
    },
    metadata(req, file, cb) {
        cb(null, { originalname: file.originalname });
    },
    root: 'ctFiles' //root name for collection to store files into
});

// const upload = util.promisify(multer({ storage: storage }).upload.any())
let upload = multer({ //multer settings for single upload
    storage: storage
}).single('file');

upload = util.promisify(upload)

module.exports = app => {
    /** API path that will upload the files */
    app.post('/upload', async (req, res) => {
        try {
            await upload(req, res);
            return universalFunctions.sendSuccess({
                statusCode: 200,
                message: responseMessages.FILE_UPLOADED_SUCCESSFULLY,
                data: req.file
            }, res);
        } catch (error) {
            return universalFunctions.sendError(error, res);
        }
    });

    app.get('/files', async (req, res) => {
        try {
            let { limit = 10, skip = 0 } = req.query
            gfs.collection('ctFiles'); //set collection name to lookup into

            let files = await gfs.files.find().limit(limit).skip(skip).toArray();
            return universalFunctions.sendSuccess({
                statusCode: 200,
                message: responseMessages.FILE_FETCHED_SUCCESSFULLY,
                data: files
            }, res);

        } catch (error) {
            return universalFunctions.sendError(error, res);
        }
    });


    app.get('/file/:filename', (req, res) => {
        try {
            gfs.collection('ctFiles'); //set collection name to lookup into

            let files = gfs.files.find({ filename: req.params.filename }).toArray();

            if (!files || files.length === 0) {
                throw Boom.badRequest(responseMessages.FILE_NOT_FOUND);
            }

            const readstream = gfs.createReadStream({
                filename: files[0].filename,
                root: "ctFiles"
            });
            // Set proper content type
            res.set('Content-Type', files[0].contentType)
            return readstream.pipe(res);
        } catch (error) {
            return universalFunctions.sendError(error, res);
        }
    });
};