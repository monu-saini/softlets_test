import users from './users.json';
import { heplers } from '../utils';

let createRecord = (knex, objectToInsert) => {
  return knex('users').insert(objectToInsert)
}

exports.seed = (knex) => {
  return knex('users').del()
    .then(() => {
      let records = [];
      users.forEach(element => {
        element.password = heplers.encrypt(element.password);
        element.created_at = new Date();
        element.updated_at = new Date();
        records.push(createRecord(knex, element))
      });
      return Promise.all(records);
    });
};