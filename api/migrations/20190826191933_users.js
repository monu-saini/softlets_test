'use strict';
exports.up = function(knex) {
    return Promise.all([
        knex.schema.createTable('users', function(table) {
          table.increments();
          table.string('email').unique();
          table.string('name');
          table.string('password');
          table.timestamps();
        })
    ]);
};

exports.down = function(knex) {
    return knex.schema.dropTable('users')
};
