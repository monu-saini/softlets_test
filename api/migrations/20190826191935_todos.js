'use strict';
exports.up = (knex) => {
    return Promise.all([
        knex.schema.createTable('todos', (table) => {
          table.increments();
          table.string('title');
          table.boolean('isCompleted').defaultTo(false);
          table.integer('userId').unsigned().references('id').inTable('users');
          table.timestamps();
        })
    ]);
};

exports.down = (knex) => {
    return knex.schema.dropTable('todos')
};
