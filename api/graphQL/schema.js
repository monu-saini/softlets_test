import { gql } from 'apollo-server-express';

// Construct a schema, using GraphQL schema language
const typeDefs = gql`
  type Query {
    todos: [Todo!]
  }

  type Mutation {
    createTodo(title: String!): Todo!
    updateTodo(id: ID!, title: String!, isCompleted: Boolean!): Todo!
    deleteTodo(id: ID!): Todo!
  }

  type Todo {
    id: ID!
    title: String!
    isCompleted: Boolean!
  }
`;

module.exports = typeDefs;

