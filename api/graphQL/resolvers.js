// Provide resolver functions for your schema fields
import { responseMessages,  universalFunctions} from '../utils';
import Joi from 'joi';
import { UserInputError } from 'apollo-server';

const resolvers = {
    Query: {
        todos: async (parent, args, { me, models }) => {
            try {
                let data = await models.Todo.query('where', 'userId', '=', me.id).fetchAll()
                return data && data.toJSON()
            } catch (error) {
                throw new Error(error);
            }
        },
    },
    Mutation: {
        createTodo: async (parent, { title }, { me, models }) => {
            try {
                const todo = {
                    title,
                    isCompleted: false,
                    userId: me.id
                };

                let schema = {
                    title: Joi.string().required()
                }

                let error = universalFunctions.validateGraphQLPayload({ title }, schema);
                if(error) throw new UserInputError(error)
                let savedData = await models.Todo.forge(todo).save();
                return savedData && savedData.toJSON();
            } catch (error) {
                console.log('[Error]', error);
                throw new Error(error);
            }
        },
        updateTodo: async (parent, { id, title , isCompleted }, { me, models }) => {
            try {
                let schema = {
                    id: Joi.number().required(),
                    title: Joi.string().required(),
                    isCompleted: Joi.boolean().required()
                }

                let error = universalFunctions.validateGraphQLPayload({ title , id, isCompleted}, schema);
                if(error) throw new UserInputError(error)

                let record = await models.Todo.query('where', 'id', '=', id).fetch();
                if(!record) throw new UserInputError(responseMessages.NOT_FOUND)
                record.set({ title , id, isCompleted})
                record.save()
                return record && record.toJSON();
            } catch (error) {
                console.log('[Error]', error);
                throw new Error(error);
            }
        },
        deleteTodo: async (parent, { id }, { me, models }) => { 
            try {
                let schema = {
                    id: Joi.number().required(),
                }

                let error = universalFunctions.validateGraphQLPayload({  id }, schema);
                if(error) throw new UserInputError(error)

                let record = await models.Todo.query('where', 'id', '=', id).fetch();
                if(!record) throw new UserInputError(responseMessages.NOT_FOUND)
                record.destroy()
                return record && record.toJSON();
            } catch (error) {
                console.log('[Error]', error);
                throw new Error(error);
            }
        }
    }
};

module.exports = resolvers;