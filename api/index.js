import { user } from './controllers';

module.exports = app => {
  //Login API
  app.post('/api/login', user.auth);
};
