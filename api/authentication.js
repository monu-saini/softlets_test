import Jwt from 'jsonwebtoken';
import Config from '../configrations';
import { User } from './models';

const jwtSecret = Config.appConfig.get('/jwtConfig/SECRET_KEY', { env: process.env.NODE_ENV });

module.exports = {
    authMiddleware(token) {
        return new Promise((resolve, reject) => {
            Jwt.verify(token, jwtSecret, async (err, decoded) => {
                if (err) {
                    reject(err)
                } else {
                    try {
                        let userInfo =  await User.where({ id: decoded.id}).fetch();
                        resolve(userInfo);
                    } catch(error) {
                        reject(error)
                    }
                }
            })
        })
    },
    createToken (payloadData, time) {
        return new Promise((resolve, reject) => {
           Jwt.sign(payloadData, jwtSecret, {
                expiresIn: time
           },(err, jwt) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(jwt)
                }
            })
        })
    },
}