import Joi from 'joi';
import { universalFunctions, responseMessages, heplers } from '../utils';
import Boom from 'boom';
import { User } from '../models';
import { createToken } from '../authentication';


module.exports = {

  async auth(req, res) {
    try  {
        const schema = Joi.object().keys({
            email: Joi.string().trim().email().required(),
            password: Joi.string().required()
        });
        
        await universalFunctions.validateRequestPayload(req.body, res, schema);

        let payload = req.body;

        let user = await User.where({
            email: payload.email,
        }).fetch();

        if (!user) {
            throw Boom.badRequest(responseMessages.USER_NOT_FOUND);
        }

        let encryptPassword = heplers.encrypt(payload.password);
        
        if (user.get('password') !== encryptPassword) {
            throw Boom.badRequest(responseMessages.INVALID_CREDENTIALS);
        }

        user = JSON.parse(JSON.stringify(user));

        delete user.password;
        delete user.created_at;
        delete user.updated_at;

        let token = await createToken({
            id: user.id
        }, "10 days");

        return universalFunctions.sendSuccess({
            statusCode: 200,
            message: responseMessages.LOGIN_SUCCESSFULLY,
            data: {
                token,
                user
            }
        }, res);
    } catch(error) {
        return universalFunctions.sendError(error, res);
    }
  },

  logout(req, res) {
    req.logout();
    return universalFunctions.sendSuccess({
        statusCode: 200,
        message: responseMessages.LOGOUT_SUCCESS,
        data: {}
    }, res);
  }
};
