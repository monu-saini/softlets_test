import { bookshelf } from './dbConnect';

let Todo = bookshelf.Model.extend({
    tableName: 'todos',
    hasTimestamps: true
});
   
module.exports = Todo