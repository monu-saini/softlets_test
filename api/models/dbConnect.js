'use strict';
import knex from 'knex';
import bookshelf from 'bookshelf';
import config from '../../knexfile';

let knexInstance = knex(config[process.env.NODE_ENV || 'development']);

let bookshelfInstance = bookshelf(knexInstance);

module.exports.bookshelf = bookshelfInstance;
module.exports.knex = knexInstance;