'use strict';

import { bookshelf } from './dbConnect';

let User = bookshelf.Model.extend({
    tableName: 'users',
    hasTimestamps: true
});
   
module.exports = User