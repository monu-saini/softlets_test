import Todo from './todos';
import User from './users';

module.exports = {
    Todo,
    User
}