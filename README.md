# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository conatains the code for todo application with following technology stack.

- React.js as the View library
- Next.js for server-side rendering of React.js
- GraphQL as the query language
- Apollo as client/server implementation of GraphQL
- PostgreSQL for storing todolist data
- MongoDB for document/file storage
- Bookshelf as node ORM for PostgreSQL 
- Joi for validation

### Prerequisite  ###
- Node JS
- MongoDB
- PostgreSQL


### How do I get set up? ###

- clone the repo
- change the directory  to softlets_test 
- npm install 
- npm run db: migrate:latest
- npm run db: seed:run
- npm start

This will run start node server on http://localhost:8080 and GraphQL server on http://localhost:8080/api/graphql

